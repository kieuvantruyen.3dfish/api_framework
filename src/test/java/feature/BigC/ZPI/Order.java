package feature.BigC.ZPI;
import core.BaseAPI;
import io.restassured.response.Response;
import io.restassured.specification.RequestSpecification;
import org.testng.annotations.BeforeClass;
import org.testng.annotations.Test;
public class Order extends BaseAPI {

    RequestSpecification requestSpecification;

    @BeforeClass
    public void beforTestOrder() {
        requestSpecification = requestSpec();
    }

    @Test
    public void getOrderByID() {
        requestSpecification.basePath("/v1/oa-shop/order-detail/1247073012200001");
        Response response = requestSpecification.get();
        System.out.println(response.asString());

    }
}
