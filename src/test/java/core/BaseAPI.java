package core;

import io.restassured.RestAssured;
import io.restassured.specification.RequestSpecification;
import org.aeonbits.owner.ConfigFactory;
import org.testng.annotations.BeforeSuite;


public abstract class BaseAPI {

    RequestSpecification requestSpecification;
    Environment environment = ConfigFactory.create(Environment.class);

    public RequestSpecification requestSpec(){
        requestSpecification = RestAssured.given();
        requestSpecification.baseUri(environment.url());
        requestSpecification.cookie(environment.cookies());
        requestSpecification.header("accept","application/json");
        return  requestSpecification;
    }


}
