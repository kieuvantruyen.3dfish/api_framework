package core;
import org.aeonbits.owner.Config.Sources;
import org.aeonbits.owner.Config;
import org.testng.annotations.BeforeSuite;

@Sources({"file:src/test/java/data/dev.properties"})
public interface Environment extends Config {
    String url();
    String cookies();
}
